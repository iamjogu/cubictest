﻿namespace Cubic.Calculator.Web.Contract
{
    public class DiagnosticReportRequest
    {
        public string Operation { get; set; }
        public string Result { get; set; }
    }
}
