﻿namespace Cubic.Calculator.Web.Contract
{
    public class SubtractRequest
    {
        public int Start { get; set; }
        public int Amount { get; set; }
    }
}
