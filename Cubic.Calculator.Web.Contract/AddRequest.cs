﻿namespace Cubic.Calculator.Web.Contract
{
    public class AddRequest
    {
        public int Start { get; set; }
        public int Amount { get; set; }
    }
}
