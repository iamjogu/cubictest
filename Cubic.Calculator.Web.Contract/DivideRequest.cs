﻿namespace Cubic.Calculator.Web.Contract
{
    public class DivideRequest
    {
        public int Start { get; set; }
        public int By { get; set; }
    }
}
