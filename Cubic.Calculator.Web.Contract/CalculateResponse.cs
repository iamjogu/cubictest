﻿namespace Cubic.Calculator.Web.Contract
{
    public class CalculateResponse
    {
        public int Result { get; set; }
    }
}
