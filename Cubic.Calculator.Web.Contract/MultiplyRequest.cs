﻿namespace Cubic.Calculator.Web.Contract
{
    public class MultiplyRequest
    {
        public int Start { get; set; }
        public int By { get; set; }
    }
}
