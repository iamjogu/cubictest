### Below are some guide notes on reviewing this coding exercise.


#### Testing The App

To test the app, set the Cubic.Calculator.App project as the startup project. 

#### Database

It was mentioned on the document that a sample SQL Server database  will be provided but none was included on the email Alyx sent me. 

What I did instead was create my own database containing a DiagnosticReport table. Inside are 3 columns namely Operation, Result, and Modified Date. Hopefully, this closely resembles original intention.

I staged this database on my personal hosting account so you can view it and see Calculator transactions being recorded on it. 

###### Please use the following credential to view database:

Connection: sql7003.site4now.net
DB Name: DB_A46CB7_cubictest
Username: DB_A46CB7_cubictest_admin
Password: sillylime10

I have setup the code (EF/Stored Proc Diagnostic) to point to this database.



#### Web Service/APIWeb Service/API

I created a WebAPI for the Web Service requirement and have also deployed it on my personal host account.

It can be accessed on the following base URL:

    http://cubic.pseapi.com/api/Calculator/

Service Endpoints are 'Add', 'Subtract', 'Multiply', and 'Divide'. So to run an Add service, simply pass request to:

    http://cubic.pseapi.com/api/Calculator/Add


###### Sample Web API use in Postman

![](https://i.imgur.com/TQRmvOb.jpg)



    All diagnostic reporting will go to the database specified above (unless modified on code).


##### Standing Up Local Web API

The console app and Web API projects are on the same solution. 

If you want to run the WebService locally, copy the entire solutions folder to another location, set Cubic.Calculator.Web project as the startup project on that other Visual Studio solution, and run the service side by side with the console app (which would be running on another instance of VS, on a different folder).

The "CalculatorEndpointUrl" config value on the console app's App.config has to be changed to reflect the localhost's address.

##### Changing Diagnostic Service

To make the APP write the diagnostic data to either console or DB (stored proc or EF), simply comment/uncomment desired implementation on Program.cs CompositionRoot method, under Cubic.Calculator.App project.

To make the Web API to write diagnostic data on DB using either Store Proc or EF approach, modify the content of AutofacConfig.cs under Cubic.Calculator.Web.App_Start folder and follow same instruction with APP above.

