﻿using System;
using Cubic.Calculator.Web.Contract;

namespace Cubic.Calculator.Web.Services
{
    public class RequestBuilder : IRequestBuilder
    {
        public AddRequest BuildAddRequest(int start, int amount)
        {
            return new AddRequest()
            {
                Start = start,
                Amount = amount
            };
        }

        public DivideRequest BuildDivideRequest(int start, int by)
        {
            return new DivideRequest()
            {
                Start = start,
                By = by
            };
        }

        public MultiplyRequest BuildMultiplyRequest(int start, int by)
        {
            return new MultiplyRequest()
            {
                Start = start,
                By = by
            };
        }

        public SubtractRequest BuildSubtractRequest(int start, int amount)
        {
            return new SubtractRequest()
            {
                Start = start,
                Amount = amount
            };
        }
    }
}
