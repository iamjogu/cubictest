﻿using Cubic.Calculator.Web.Services.Api;

namespace Cubic.Calculator.Web.Services
{
    public class ApiClientFactory : IApiClientFactory
    {
        IApiClient IApiClientFactory.Create()
        {
            return new ApiClient();
        }
    }
}
