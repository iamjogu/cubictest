﻿using System.Configuration;
using System.Threading.Tasks;
using Cubic.Calculator.Web.Contract;

namespace Cubic.Calculator.Web.Services
{
    public class WebCalculatorService : IWebCalculatorService
    {
        private readonly IApiClientFactory _apiClientFactory;
        private readonly string baseUrl = ConfigurationManager.AppSettings["CalculatorEndpointUrl"];

        public WebCalculatorService(IApiClientFactory apiClientFactory)
        {
            _apiClientFactory = apiClientFactory;
        }

        public async Task<int> AddAsync(AddRequest request)
        {
            var apiClient = _apiClientFactory.Create();
            var result = await apiClient.Post<AddRequest, CalculateResponse>(baseUrl + "Add", request);
            return result.Response.Result;
        }

        public async Task<int> DivideAsync(DivideRequest request)
        {
            var apiClient = _apiClientFactory.Create();
            var result = await apiClient.Post<DivideRequest, CalculateResponse>(baseUrl + "Divide", request);
            return result.Response.Result;
        }

        public async Task<int> MultiplyAsync(MultiplyRequest request)
        {
            var apiClient = _apiClientFactory.Create();
            var result = await apiClient.Post<MultiplyRequest, CalculateResponse>(baseUrl + "Multiply", request);
            return result.Response.Result;
        }

        public async Task<int> SubtractAsync(SubtractRequest request)
        {
            var apiClient = _apiClientFactory.Create();
            var result = await apiClient.Post<SubtractRequest, CalculateResponse>(baseUrl + "Subtract", request);
            return result.Response.Result;
        }
    }
}
