﻿using System;
using Cubic.Calculator.Web.Contract;

namespace Cubic.Calculator.Web.Services
{
    public class ResponseBuilder : IResponseBuilder
    {
        public CalculateResponse BuildCalculateResponse(int result)
        {
            return new CalculateResponse()
            {
                Result = result
            };
        }
    }
}
