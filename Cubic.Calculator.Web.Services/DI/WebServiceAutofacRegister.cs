﻿using Autofac;
using Cubic.Calculator.Web.Services.Api;

namespace Cubic.Calculator.Web.Services.DI
{
    public static class WebServiceAutofacRegister
    {
        public static ContainerBuilder ContainerBuilder { get; set; }

        public static void Register(ContainerBuilder builder)
        {
            ContainerBuilder = builder;

            builder.RegisterType<JsonSerializer>().As<ISerializer>();
            builder.RegisterType<ApiClientFactory>().As<IApiClientFactory>();
            builder.RegisterType<ApiClient>().As<IApiClient>();
            builder.RegisterType<RequestBuilder>().As<IRequestBuilder>();
            builder.RegisterType<ResponseBuilder>().As<IResponseBuilder>();
            builder.RegisterType<WebCalculatorService>().As<IWebCalculatorService>();
        }
    }
}
