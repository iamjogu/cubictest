﻿using Cubic.Calculator.Web.Services.Api;

namespace Cubic.Calculator.Web.Services
{
    public interface IApiClientFactory
    {
        IApiClient Create();
    }
}
