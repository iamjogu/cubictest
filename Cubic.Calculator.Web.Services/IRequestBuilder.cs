﻿using Cubic.Calculator.Web.Contract;

namespace Cubic.Calculator.Web.Services
{
    public interface IRequestBuilder
    {
        AddRequest BuildAddRequest(int start, int amount);
        SubtractRequest BuildSubtractRequest(int start, int amount);
        MultiplyRequest BuildMultiplyRequest(int start, int by);
        DivideRequest BuildDivideRequest(int start, int by);
    }
}
