﻿using Cubic.Calculator.Web.Contract;
using System.Threading.Tasks;

namespace Cubic.Calculator.Web.Services
{
    public interface IWebCalculatorService
    {
        Task<int> AddAsync(AddRequest request);
        Task<int> SubtractAsync(SubtractRequest request);
        Task<int> MultiplyAsync(MultiplyRequest request);
        Task<int> DivideAsync(DivideRequest request);
    }
}
