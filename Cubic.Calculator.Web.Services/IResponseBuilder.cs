﻿using Cubic.Calculator.Web.Contract;

namespace Cubic.Calculator.Web.Services
{
    public interface IResponseBuilder
    {
        CalculateResponse BuildCalculateResponse(int result);
    }
}
