﻿using System.Net;

namespace Cubic.Calculator.Web.Services.Api
{
    public class ApiResult<TResponse> where TResponse : class, new()
    {
        public ApiResult(string url)
        {
            Url = url;
        }

        public string Url { get; }
        public HttpStatusCode StatusCode { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public TResponse Response { get; set; }
    }
}
