﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Cubic.Calculator.Web.Services.Api
{
    public class ApiClient : IApiClient
    {
        private readonly ISerializer _serializer;

        public ApiClient()
        {
            _serializer = new JsonSerializer();
        }

        public async Task<ApiResult<TResponse>> Post<TRequest, TResponse>(string url, TRequest request, IDictionary<string, string> headers = null, bool throwUnsuccessful = true)
            where TRequest : class
            where TResponse : class, new()
        {
            var result = new ApiResult<TResponse>(url);

            try
            {
                result.RequestBody = _serializer.Serialize(request);

                using (var httpClient = new HttpClient())
                {
                    var requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
                    var response = await httpClient.PostAsJsonAsync(url, request);

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception("Post request unsuccessful");
                    }

                    result.StatusCode = response.StatusCode;
                    result.ResponseBody = await response.Content.ReadAsStringAsync();
                    result.Response = _serializer.Deserialize<TResponse>(result.ResponseBody);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void AddHeaders(HttpRequestMessage requestMessage, IDictionary<string, string> headers)
        {
            if (headers != null)
            {
                foreach (var kvp in headers)
                {
                    requestMessage.Headers.Add(kvp.Key, kvp.Value);
                }
            }
        }
    }
}
