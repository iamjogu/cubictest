﻿namespace Cubic.Calculator.Web.Services.Api
{
    public interface ISerializer
    {
        string Serialize<TInput>(TInput input) where TInput : class;
        TOutput Deserialize<TOutput>(string output) where TOutput : class, new();
    }
}
