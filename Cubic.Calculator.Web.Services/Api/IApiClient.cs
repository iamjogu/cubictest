﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cubic.Calculator.Web.Services.Api
{
    public interface IApiClient
    {
        Task<ApiResult<TResponse>> Post<TRequest, TResponse>(string url, TRequest request, IDictionary<string, string> headers = null, bool throwUnsuccessful = true)
            where TRequest : class where TResponse : class, new();
    }
}
