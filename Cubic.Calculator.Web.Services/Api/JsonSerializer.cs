﻿using Newtonsoft.Json;

namespace Cubic.Calculator.Web.Services.Api
{
    public class JsonSerializer : ISerializer
    {
        public TOutput Deserialize<TOutput>(string output) where TOutput : class, new()
        {
            return JsonConvert.DeserializeObject<TOutput>(output);
        }

        public string Serialize<TInput>(TInput input) where TInput : class
        {
            return JsonConvert.SerializeObject(input);
        }
    }
}
