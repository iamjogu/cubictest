﻿using Autofac;
using Cubic.Calculator.Data.Repositories;
using Cubic.Calculator.Data.Services;

namespace Cubic.Calculator.Data.DI
{
    public static class DataAutofacRegister
    {
        public static ContainerBuilder ContainerBuilder { get; set; }

        public static void Register(ContainerBuilder builder)
        {
            ContainerBuilder = builder;

            builder.RegisterType<DiagnosticReportBuilder>().As<IDiagnosticReportBuilder>();
            builder.RegisterType<GenericRepository>().As<IGenericRepository>();
        }
    }
}
