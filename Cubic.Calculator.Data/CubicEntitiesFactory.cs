﻿namespace Cubic.Calculator.Data
{
    public class CubicEntitiesFactory : ICubicEntitiesFactory
    {
        public ICubicEntities Create()
        {
            return new CubicEntities();
        }
    }
}
