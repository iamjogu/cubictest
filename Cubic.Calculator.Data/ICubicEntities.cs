﻿using System;
using System.Data.Entity;

namespace Cubic.Calculator.Data
{
    public interface ICubicEntities : IDisposable
    {
        DbSet<DiagnosticReport> DiagnosticReports { get; set; }
    }
}
