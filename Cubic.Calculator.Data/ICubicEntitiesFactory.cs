﻿namespace Cubic.Calculator.Data
{
    public interface ICubicEntitiesFactory
    {
        ICubicEntities Create();
    }
}
