//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cubic.Calculator.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DiagnosticReport
    {
        public int Id { get; set; }
        public string Operation { get; set; }
        public string Result { get; set; }
        public System.DateTimeOffset ModifiedDate { get; set; }
    }
}
