﻿using System;

namespace Cubic.Calculator.Data.Services
{
    public class DiagnosticReportBuilder : IDiagnosticReportBuilder
    {
        public DiagnosticReport CreateDiagnosticReport(string operation, string result)
        {
            return new DiagnosticReport()
            {
                Operation = operation,
                Result = result.ToString(),
                ModifiedDate = DateTimeOffset.UtcNow
            };
        }
    }
}
