﻿namespace Cubic.Calculator.Data.Services
{
    public interface IDiagnosticReportBuilder
    {
        DiagnosticReport CreateDiagnosticReport(string operation, string result);
    }
}
