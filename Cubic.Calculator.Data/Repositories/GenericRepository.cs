﻿namespace Cubic.Calculator.Data.Repositories
{
    public class GenericRepository : IGenericRepository
    {
        public void Add<T>(T entity) where T : class, new()
        {
            using (var db = new CubicEntities())
            {
                db.Set<T>().Add(entity);
                db.SaveChanges();
            }
        }
    }
}