﻿namespace Cubic.Calculator.Data.Repositories
{
    public interface IGenericRepository
    {
        void Add<T>(T entity) where T : class, new();
    }
}
