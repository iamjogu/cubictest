﻿using Autofac;
using Cubic.Calculator.Common;
using Cubic.Calculator.Data.DI;
using Cubic.Calculator.Web.Services.DI;

namespace Cubic.Calculator.App
{
    public class Program
    {
        static private IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterType<SimpleCalculator>().As<ISimpleCalculator>();
            //builder.RegisterType<DbStoredProcDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("storedproc");
            builder.RegisterType<DbEFDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("dbef");
            //builder.RegisterType<DummyDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("dummy");
            //builder.RegisterType<ConsoleDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("console");

            WebServiceAutofacRegister.Register(builder);
            DataAutofacRegister.Register(builder);
            return builder.Build();
        }

        static void Main(string[] args)
        {
            CompositionRoot().Resolve<Application>().Run();
        }
    }
}