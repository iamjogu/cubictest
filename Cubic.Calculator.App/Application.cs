﻿using Cubic.Calculator.Common;
using Cubic.Calculator.Web.Services;
using System;

namespace Cubic.Calculator.App
{
    public class Application
    {
        private readonly ISimpleCalculator _simpleCalculator;
        private readonly IWebCalculatorService _webCalculatorService;
        private readonly IRequestBuilder _requestBuilder;
        
        public Application(ISimpleCalculator simpleCalculator, 
            IWebCalculatorService webCalculatorService, IRequestBuilder requestBuilder)
        {
            _simpleCalculator = simpleCalculator;
            _webCalculatorService = webCalculatorService;
            _requestBuilder = requestBuilder;
        }

        public void Run()
        {
            try
            {
                var selection = 0;
                var continueLoop = true;

                while (continueLoop)
                {
                    Console.WriteLine("Select an operation");
                    Console.WriteLine("(1) Add");
                    Console.WriteLine("(2) Subtract");
                    Console.WriteLine("(3) Multiply");
                    Console.WriteLine("(4) Divide");
                    Console.WriteLine("(5) Exit");
                    Console.Write(">>");
                    selection = Convert.ToInt32(Console.ReadLine());

                    switch (selection)
                    {
                        case 1:
                            Add();
                            break;
                        case 2:
                            Subtract();
                            break;
                        case 3:
                            Multiply();
                            break;
                        case 4:
                            Divide();
                            break;
                        case 5:
                            continueLoop = false;
                            break;
                        default:
                            Console.WriteLine("Invalid option, please try again!");
                            break;
                    }
                };
            }
            catch (Exception ex )
            {
                Console.WriteLine("An error occurred! Application will now terminate. Press any key to end.");
                Console.WriteLine($"Exception Details: {ex}");
                Console.Read();
            }
        }
        
        private void Add()
        {
            Console.Write("Start value: ");
            var val1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Amount to add: ");
            var val2 = Convert.ToInt32(Console.ReadLine());
            //_simpleCalculator.Add(val1, val2);
            _webCalculatorService.AddAsync(_requestBuilder.BuildAddRequest(val1, val2));
        }

        private void Subtract()
        {
            Console.Write("Start value: ");
            var start = Convert.ToInt32(Console.ReadLine());
            Console.Write("Amount to subtract: ");
            var amount = Convert.ToInt32(Console.ReadLine());
            //_simpleCalculator.Subtract(start, amount);
            _webCalculatorService.SubtractAsync(_requestBuilder.BuildSubtractRequest(start, amount));
        }

        private void Multiply()
        {
            Console.Write("Start value: ");
            var start = Convert.ToInt32(Console.ReadLine());
            Console.Write("Multiply by: ");
            var by = Convert.ToInt32(Console.ReadLine());
            //_simpleCalculator.Multiply(start, by);
            _webCalculatorService.MultiplyAsync(_requestBuilder.BuildMultiplyRequest(start, by));
        }

        private void Divide()
        {
            Console.Write("Start value: ");
            var start = Convert.ToInt32(Console.ReadLine());
            Console.Write("Divide by: ");
            var by = Convert.ToInt32(Console.ReadLine());
            //_simpleCalculator.Divide(start, by);
            _webCalculatorService.DivideAsync(_requestBuilder.BuildDivideRequest(start, by));
        }
    }
}
