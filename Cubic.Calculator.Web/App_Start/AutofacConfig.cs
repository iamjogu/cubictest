﻿using Autofac;
using Autofac.Integration.WebApi;
using Cubic.Calculator.Common;
using Cubic.Calculator.Data.DI;
using Cubic.Calculator.Web.Services.DI;
using System.Reflection;
using System.Web.Http;

namespace Cubic.Calculator.Web.App_Start
{
    public class AutofacConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<SimpleCalculator>().As<ISimpleCalculator>();
            builder.RegisterType<DbStoredProcDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("storedproc");
            //builder.RegisterType<DbEFDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("dbef");
            //builder.RegisterType<DummyDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("dummy");
            //builder.RegisterType<ConsoleDiagnosticService>().As<IDiagnosticService>().Keyed<IDiagnosticService>("console");

            WebServiceAutofacRegister.Register(builder);
            DataAutofacRegister.Register(builder);

            Container = builder.Build();
            return Container;
        }
    }
}