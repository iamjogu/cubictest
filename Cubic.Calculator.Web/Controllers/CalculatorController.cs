﻿using Cubic.Calculator.Common;
using Cubic.Calculator.Web.Contract;
using Cubic.Calculator.Web.Services;
using System.Web.Http;

namespace Cubic.Calculator.Web.Controllers
{
    public class CalculatorController : ApiController
    {
        private readonly ISimpleCalculator _simpleCalculator;
        private readonly IResponseBuilder _responseBuilder;

        public CalculatorController(ISimpleCalculator simpleCalculator, IResponseBuilder responseBuilder)
        {
            _simpleCalculator = simpleCalculator;
            _responseBuilder = responseBuilder;
        }

        [Route("api/Calculator/Add")]
        [HttpPost]
        public CalculateResponse Add(AddRequest request)
        {
            return _responseBuilder.BuildCalculateResponse(_simpleCalculator.Add(request.Start, request.Amount));
        }

        [Route("api/Calculator/Subtract")]
        [HttpPost]
        public CalculateResponse Subtract(SubtractRequest request)
        {
            return _responseBuilder.BuildCalculateResponse(_simpleCalculator.Subtract(request.Start, request.Amount));
        }

        [Route("api/Calculator/Multiply")]
        [HttpPost]
        public CalculateResponse Multiply(MultiplyRequest request)
        {
            return _responseBuilder.BuildCalculateResponse(_simpleCalculator.Multiply(request.Start, request.By));
        }

        [Route("api/Calculator/Divide")]
        [HttpPost]
        public CalculateResponse Divide(DivideRequest request)
        {
            return _responseBuilder.BuildCalculateResponse(_simpleCalculator.Divide(request.Start, request.By));
        }
    }
}