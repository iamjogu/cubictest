﻿using Cubic.Calculator.Common;
using Cubic.Calculator.Web.Contract;
using System;
using System.Web.Http;

namespace Cubic.Calculator.Web.Controllers
{
    public class DiagnosticController : ApiController
    {
        private readonly IDiagnosticService _diagnosticService;

        public DiagnosticController(IDiagnosticService diagnosticService)
        {
            _diagnosticService = diagnosticService;
        }

        // POST api/<controller>
        [HttpPost]
        public void DiagnosticReport(DiagnosticReportRequest request)
        {
            try
            {
                _diagnosticService.GenerateReport(request.Operation, Convert.ToInt32(request.Result));
            }
            catch (System.Exception)
            {
                throw; //TODO: throw 500 for catchall
            }
        }
    }
}