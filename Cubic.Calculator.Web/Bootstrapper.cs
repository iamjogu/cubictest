﻿using Cubic.Calculator.Web.App_Start;
using System.Web.Http;

namespace Cubic.Calculator.Web
{
    public class Bootstrapper
    {
        public static void Run()
        {
            AutofacConfig.Initialize(GlobalConfiguration.Configuration);
        }
    }
}