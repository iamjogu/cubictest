﻿namespace Cubic.Calculator.Common
{
    public interface IDiagnosticService
    {
        void GenerateReport(string operation, int result);
    }
}