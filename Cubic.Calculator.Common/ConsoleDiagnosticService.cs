﻿namespace Cubic.Calculator.Common
{
    public class ConsoleDiagnosticService : IDiagnosticService
    {
        public void GenerateReport(string operation, int result)
        {
            System.Console.WriteLine($"{operation} = {result}");
        }
    }
}
