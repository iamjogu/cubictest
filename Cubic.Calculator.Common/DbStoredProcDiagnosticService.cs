﻿using Cubic.Calculator.Data;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace Cubic.Calculator.Common
{
    public class DbStoredProcDiagnosticService : IDiagnosticService
    {
        public void GenerateReport(string operation, int result)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ReportDatabase"].ConnectionString;
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(DataConstants.AddReportStoredProcName, conn))
            {
                conn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Operation", operation));
                cmd.Parameters.Add(new SqlParameter("@Result", result));
                cmd.Parameters.Add(new SqlParameter("@Modified", DateTimeOffset.UtcNow));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
