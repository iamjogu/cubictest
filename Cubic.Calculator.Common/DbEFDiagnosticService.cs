﻿using Cubic.Calculator.Data;
using Cubic.Calculator.Data.Repositories;
using Cubic.Calculator.Data.Services;

namespace Cubic.Calculator.Common
{
    public class DbEFDiagnosticService : IDiagnosticService
    {
        private readonly IGenericRepository _genericRepository;
        private readonly IDiagnosticReportBuilder _diagnosticReportBuilder;

        public DbEFDiagnosticService(IGenericRepository genericRepository, 
            IDiagnosticReportBuilder reportBuilder)
        {
            _genericRepository = genericRepository;
            _diagnosticReportBuilder = reportBuilder;
        }

        public void GenerateReport(string operation, int result)
        {
            var diagnosticReport = _diagnosticReportBuilder.CreateDiagnosticReport(operation, result.ToString());

            _genericRepository.Add<DiagnosticReport>(diagnosticReport);
        }
    }
}
