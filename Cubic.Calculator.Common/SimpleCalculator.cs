﻿namespace Cubic.Calculator.Common
{
    public class SimpleCalculator : ISimpleCalculator
    {
        private readonly IDiagnosticService _diagnosticService;

        public SimpleCalculator(IDiagnosticService diagnosticService)
        {
            _diagnosticService = diagnosticService;
        }

        public int Add(int start, int amount)
        {
            var result = start + amount;
            _diagnosticService.GenerateReport($"{start} + {amount}", result);
            return result;
        }

        public int Divide(int start, int by)
        {
            var result = start / by;
            _diagnosticService.GenerateReport($"{start} / {by}", result);
            return result;
        }

        public int Multiply(int start, int by)
        {
            var result = start * by;
            _diagnosticService.GenerateReport($"{start} * {by}", result);
            return result;
        }

        public int Subtract(int start, int amount)
        {
            var result = start - amount;
            _diagnosticService.GenerateReport($"{start} - {amount}", result);
            return result;
        }
    }
}
