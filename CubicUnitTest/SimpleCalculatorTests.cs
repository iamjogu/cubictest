﻿using System;
using Cubic.Calculator.Common;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace CubicUnitTest
{
    [TestFixture]
    public class SimpleCalculatorTests
    {
        private Mock<IDiagnosticService> _mockDiagnosticService;
        private SimpleCalculator _sut;

        [SetUp]
        public void Setup()
        {
            _mockDiagnosticService = new Mock<IDiagnosticService>();
            _sut = new SimpleCalculator(_mockDiagnosticService.Object);
        }

        [TestCase(1, 2, 3)]
        [TestCase(-1, -2, -3)]
        [TestCase(-10, 20, 10)]
        [TestCase(33, -10, 23)]
        public void Add_ValidValuesProvided_CorrectValueReturned(int start, int amount, int output)
        {
            var result = _sut.Add(start, amount);

            this.ShouldSatisfyAllConditions(
                () => result.ShouldBe(output),
                () => _mockDiagnosticService.Verify(x => x.GenerateReport($"{start} + {amount}", output), Times.Once)
            );
            Assert.AreEqual(output, result);
        }

        [TestCase(100, 1, 100)]
        [TestCase(10, -10, -1)]
        [TestCase(-33, 11, -3)]
        [TestCase(-56, -8, 7)]
        public void Divide_ValidValuesProvided_CorrectValuesReturned(int start, int by, int output)
        {
            var result = _sut.Divide(start, by);

            this.ShouldSatisfyAllConditions(
                () => result.ShouldBe(output),
                () => _mockDiagnosticService.Verify(x => x.GenerateReport($"{start} / {by}", output), Times.Once)
            );
        }

        [TestCase(9, 4, 2)]
        [TestCase(-12, -5, 2)]
        [TestCase(890, 32, 27)]
        public void Divide_ResultsToNonWholeNumber_OnlyWholeNumberPartReturned(int start, int by, int output)
        {
            var result = _sut.Divide(start, by);

            this.ShouldSatisfyAllConditions(
                () => result.ShouldBe(output),
                () => _mockDiagnosticService.Verify(x => x.GenerateReport($"{start} / {by}", output), Times.Once)
            );
        }

        [TestCase(3, 0)]
        public void Divide_ByZero_DivisionByZeroExceptionThrown(int start, int by)
        {
            var exception = Should.Throw<DivideByZeroException>(() => _sut.Divide(start, by));

            this.ShouldSatisfyAllConditions(
                () => exception.ShouldNotBeNull(),
                () => exception.ShouldBeOfType<DivideByZeroException>(),
                () => _mockDiagnosticService.Verify(x => x.GenerateReport(It.IsAny<string>(), It.IsAny<int>()), Times.Never)
            );
        }

        [TestCase(1, 1, 1)]
        [TestCase(-10, 1, -10)]
        [TestCase(7, -8, -56)]
        [TestCase(-100, -33, 3300)]
        [TestCase(90, 45, 4050)]
        public void Multiply_ValidValuesProvided_CorrectValuesReturned(int start, int by, int output)
        {
            var result = _sut.Multiply(start, by);

            this.ShouldSatisfyAllConditions(
                () => result.ShouldBe(output),
                () => _mockDiagnosticService.Verify(x => x.GenerateReport($"{start} * {by}", output), Times.Once)
            );
        }

        [TestCase(1, 1, 0)]
        [TestCase(-10, 4, -14)]
        [TestCase(54, -4, 58)]
        [TestCase(-3, -3, 0)]
        public void Subtract_ValidValuesProvided_CorrectValuesReturned(int start, int amount, int output)
        {
            var result = _sut.Subtract(start, amount);

            this.ShouldSatisfyAllConditions(
                () => result.ShouldBe(output),
                () => _mockDiagnosticService.Verify(x => x.GenerateReport($"{start} - {amount}", output), Times.Once)
            );
        }
    }
}